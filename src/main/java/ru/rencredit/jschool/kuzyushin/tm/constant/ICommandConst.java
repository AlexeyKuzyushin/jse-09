package ru.rencredit.jschool.kuzyushin.tm.constant;

public interface ICommandConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String INFO = "info";

    String EXIT = "exit";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";
}
