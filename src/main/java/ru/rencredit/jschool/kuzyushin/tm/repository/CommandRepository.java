package ru.rencredit.jschool.kuzyushin.tm.repository;

import ru.rencredit.jschool.kuzyushin.tm.api.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.constant.IArgumentConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.ICommandConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.IDescriptionConst;
import ru.rencredit.jschool.kuzyushin.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            ICommandConst.HELP, IArgumentConst.HELP, IDescriptionConst.HELP
    );

    public static final TerminalCommand ARGUMENTS = new TerminalCommand(
            ICommandConst.ARGUMENTS, IArgumentConst.ARGUMENTS, IDescriptionConst.ARGUMENTS
    );

    public static final TerminalCommand COMMANDS = new TerminalCommand(
            ICommandConst.COMMANDS, IArgumentConst.COMMANDS, IDescriptionConst.COMMANDS
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            ICommandConst.ABOUT, IArgumentConst.ABOUT, IDescriptionConst.ABOUT
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            ICommandConst.VERSION, IArgumentConst.VERSION, IDescriptionConst.VERSION
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            ICommandConst.INFO, IArgumentConst.INFO, IDescriptionConst.INFO
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            ICommandConst.EXIT, null, IDescriptionConst.EXIT
    );

    private final String[] ARRAY_ARGUMENTS = getArguments(TERMINAL_COMMANDS);

    private final String[] ARRAY_COMMANDS = getCommands(TERMINAL_COMMANDS);

    private static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS, EXIT
    };

    public String[] getArguments(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getArguments() {
        return ARRAY_ARGUMENTS;
    }

    public String[] getCommands() {
        return ARRAY_COMMANDS;
    }
}
