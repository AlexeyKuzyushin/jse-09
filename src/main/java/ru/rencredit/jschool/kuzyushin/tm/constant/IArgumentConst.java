package ru.rencredit.jschool.kuzyushin.tm.constant;

public interface IArgumentConst {

    String HELP = "-h";

    String VERSION = "-v";

    String ABOUT = "-a";

    String INFO = "-i";

    String ARGUMENTS = "-arg";

    String COMMANDS = "-cmd";
}
