package ru.rencredit.jschool.kuzyushin.tm.constant;

public interface IDescriptionConst {

    String HELP = " - Display terminal commands.";

    String VERSION = "Show version info.";

    String ABOUT = "Show developer info.";

    String INFO = "Show system info";

    String EXIT = "Close application";

    String ARGUMENTS = "Show program arguments";

    String COMMANDS = "Show program commands";
}
