package ru.rencredit.jschool.kuzyushin.tm;

import ru.rencredit.jschool.kuzyushin.tm.constant.IArgumentConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.ICommandConst;
import ru.rencredit.jschool.kuzyushin.tm.model.TerminalCommand;
import ru.rencredit.jschool.kuzyushin.tm.repository.CommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.util.INumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    public static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ICommandConst.VERSION:
                showVersion();
                break;
            case IArgumentConst.ABOUT:
                showAbout();
                break;
            case IArgumentConst.HELP:
                showHelp();
                break;
            case IArgumentConst.INFO:
                showInfo();
                break;
            case IArgumentConst.ARGUMENTS:
                showArguments();
                break;
            case IArgumentConst.COMMANDS:
                showCommands();
                break;
            default:
                showArgErrorMessage(arg);
        }
    }

    private static void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case ICommandConst.VERSION:
                showVersion();
                break;
            case ICommandConst.ABOUT:
                showAbout();
                break;
            case ICommandConst.HELP:
                showHelp();
                break;
            case ICommandConst.INFO:
                showInfo();
                break;
            case ICommandConst.EXIT:
                exit();
                break;
            case ICommandConst.ARGUMENTS:
                showArguments();
                break;
            case ICommandConst.COMMANDS:
                showCommands();
                break;
            default:
                showCmdErrorMessage(command);
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showArgErrorMessage(final String arg) {
        System.out.println("'" + arg + "' is not a Task Manager argument. See '-h'");
    }
    private static void showCmdErrorMessage(final String arg) {
        System.out.println("'" + arg + "' is not a Task Manager command. See 'help'");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Alexey Kuzyushin");
        System.out.println("E-MAIL: alexeykuzyushin@yandex.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final TerminalCommand command: commands) System.out.println(command);
    }

    private static void showInfo() {
        System.out.println("[INFO]");

        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = INumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = INumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = INumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = INumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

    private static void showCommands() {
        final String[] commands = COMMAND_REPOSITORY.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    private static void showArguments() {
        final String[] arguments = COMMAND_REPOSITORY.getArguments();
        System.out.println(Arrays.toString(arguments));
    }
}
